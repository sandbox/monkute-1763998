api = 2
core = 7.x

; Dependencies =================================================================

projects[ctools][type] = module
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.x-dev
projects[ctools][download][type] = git
projects[ctools][download][url] = http://git.drupal.org/project/ctools.git
projects[ctools][download][branch] = 7.x-1.x

projects[entity][type] = module
projects[entity][subdir] = contrib
projects[entity][version] = 1.x-dev
projects[entity][download][type] = git
projects[entity][download][url] = http://git.drupal.org/project/entity.git
projects[entity][download][branch] = 7.x-1.x

projects[rules][type] = module
projects[rules][subdir] = contrib
projects[rules][version] = 2.x-dev
projects[rules][download][type] = git
projects[rules][download][url] = http://git.drupal.org/project/rules.git
projects[rules][download][branch] = 7.x-2.x

projects[views][type] = module
projects[views][subdir] = contrib
projects[views][version] = 3.x-dev
projects[views][download][type] = git
projects[views][download][url] = http://git.drupal.org/project/views.git
projects[views][download][branch] = 7.x-3.x

; Drupal Ubercart and Ubercart contribs ========================================

projects[ubercart][type] = module
projects[ubercart][subdir] = contrib
projects[ubercart][version] = 1.x-dev
projects[ubercart][download][type] = git
projects[ubercart][download][url] = http://git.drupal.org/project/ubercart.git
projects[ubercart][download][branch] = 7.x-1.x

projects[panels][type] = module
projects[panels][subdir] = contrib
projects[panels][version] = 1.x-dev
projects[panels][download][type] = git
projects[panels][download][url] = http://git.drupal.org/project/panels.git
projects[panels][download][branch] = 7.x-1.x

projects[views_slideshow][type] = module
projects[views_slideshow][subdir] = contrib
projects[views_slideshow][version] = 1.x-dev
projects[views_slideshow][download][type] = git
projects[views_slideshow][download][url] = http://git.drupal.org/project/views_slideshow.git
projects[views_slideshow][download][branch] = 7.x-1.x

projects[features][type] = module
projects[features][subdir] = contrib
projects[features][version] = 1.x-dev
projects[features][download][type] = git
projects[features][download][url] = http://git.drupal.org/project/features.git
projects[features][download][branch] = 7.x-1.x

projects[devel][type] = module
projects[devel][subdir] = contrib
projects[devel][version] = 1.x-dev
projects[devel][download][type] = git
projects[devel][download][url] = http://git.drupal.org/project/devel.git
projects[devel][download][branch] = 7.x-1.x
﻿CA´CH CA`I ÐA?T VA` SU? DU?NG TALO

1.Ca´c buo´c ca`i da?t Talo tu` Drupal:

	- Va`o kho ma~ nguô`n cu?a du? a´n MHST12-13: Talo – Ta?o website ba´n ha`ng vo´i mô?t 	click: http://drupal.org/node/1763998/commits. Thâ´y phâ`n “source code” dê? su? du?ng khi va`o GitBash dê? ta?i vê` ma´y.

	- Va`o noi ô? di~a muô´n luu trang web ta?o 1 thu mu?c co´ tên “Talo downloads” . Sau do´ nhâ´n chuô?t pha?i , cho?n phâ`n GitBash  dê? chi? dê´n du´ng duo`ng dâ~n cu?a no´ trong 	GitBash.
	- Sau do´, coppy hoa?c go~ lê?nh:  
	git clone http://git.drupal.org/sandbox/monkute/1763998.git talo

 	- Va`o trong Gitbash va` nhâ´n Enter la` no´ se~ tu? ta?i vê` ma´y va`o du´ng thu mu?c vu`a ta?o o? trên.

	- Sau khi ta?i xong thi` thoa´t kho?i GitBash va` ra thu mu?c luu Talo downloads dê? ba´t dâ`u ca`i da?t.(Luu y´: câ`n cop tâ´t ca? ca´c tha`nh phâ`n trang cu?a talo ra tru?c tiê´p trên thu mu?c Talo downloads thi` mo´i co´ thê? ca´i da?t duo?c).

 	- Va`o file httpd-vhost.conf trong thu mu?c ca`i dat “Apache/conf/extra”. Va` 	coppy phâ`n “code” dê? truy câ?p va`o di?a chi? trang web.
 
	Vi´ du?:
	<VirtualHost *:80>
    	ServerAdmin webmaster@local.hleclerc-PC.ingenidev      
    	DocumentRoot "f:/lam web/Talo downloads"
    	ServerName local.talo
    	ServerAlias local2.talo
    	ErrorLog "logs/talo-error.log"
   	 CustomLog "logs/talo-access.log" common
	</VirtualHost>
	<Directory "F:/lam web">
    	AllowOverride All
   	Options Indexes FollowSymLinks
    	Order allow,deny
    	Allow from all
	</Directory>

	- Va`o file host trong windows dê? thêm di?a chi? truy câ?p va`o dâ`u trang. Vi´ du? nhu sau:
	127.0.0.1local.talo local2.talo

	- Sau khi xong, luu la?i tâ´t ca? va` va`o Apache kho?i dô?ng la?i Apache va`o tri`nh 	duyê?t va` tiê´n ha`nh ca`i da?t lâ`n luo?t theo ca´c buo´c nhu sau:

	• Khi ma`n hi`nh ca`i da?t hiê?n lên ca´c radio bottom cho?n ca´c ca´ch ca`i da?t cho 	trang web. Cho?n phâ`n cuô´i cu`ng co´ tên Talo va` click va`o Save and cotinnue

	• Ðê´n phâ`n cho?n ngôn ngu~: Cho?n ngôn ngu~ Tiê´ng Viê?t nê´u ba?n không su? du?ng tha`nh tha?o duo?c tiê´ng Anh.

	• Tiê´p theo dê´n phâ`n diê?n thông tin vê` tên database, user, pass cu?a php myadmin. Phâ`n ca`i da?t nâng cao thi` pha?i diê`n va`o phâ`n cuô´i 1 tên da?c trung cu?a 	project vi´ du? nhu: talo_
 	Khi drupal thông ba´o da~ ca`i da?t duo?c rô`i .Ba?n co´ thê? xem trang hiê?n ta?i. 
	- Sau khi va`o duo?c trang hiê?n ta?i thi` ba?n câ`n dang nhâ?p.

2.Ca´ch su? du?ng

	- Va`o duo?c trang web thi` ba?n thâ´y co´ phâ`n : thêm sa?n phâ?m mo´i, ba?n co´ thê? thêm sa?n phâ?m vo´i ca´c thông tin nhu yêu câ`u. Ca´c sa?n phâ?m duo?c thêm se~ hiê?n thi? lên 	trang chu? cu?a web.

	- Vi` chu´c nang cu?a Talo la` mô? trang web co ba?n nên chi? da´p u´ng duo?c nhu~ng chu´c nang chi´nh cu?a 1 trang web ba´n ha`ng. Tâ´t ca? ca´c phâ`n ca`i da?t câ´u hi`nh, mô dun,hay giao diê?n,… dê`u co´ huo´ng dâ~n trên drupal. Nê´u muô´n ba?n co´ thê? thay dô?i cho phu` ho?p nhâ´t. 
